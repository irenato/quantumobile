<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 22:40
 */

/**
 * Template name: Vacancies
 */

get_header();

?>

<?php if (have_posts()) : while (have_posts()) :
    the_post(); ?>

    <section class="vacancies">
        <div class="wrapper">
            <div class="vacancies-header">
                <a href="<?= get_the_permalink(41) ?>" class="back-link"><i class="fa fa-chevron-left" aria-hidden="true"></i> back</a>
                <p class="post-date">Posted on: <?= get_the_date('F d, Y') ?></p>
            </div>
            <div class="vacancies-title">
                <h1 class="vacancies-name"><?php the_title() ?></h1>
                <p class="vacancies-place"><?= get_field('location') ?></p>
                <a href="#vacanciesType" class="link-btn blue"><?= get_field('type') ?></a>
            </div>
            <div class="vacancies-description">
                <?php the_content() ?>
            </div>
            <a href="<?= get_the_permalink(10) ?>" class="link-btn white">aply for job</a>

        </div>
    </section>

<?php endwhile; ?>
<?php endif; ?>

<?php

get_footer();
