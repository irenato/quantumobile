$(function(){

    $('a[href^="#"]').click(function () {
        elementClick = $(this).attr("href");
        destination = $(elementClick).offset().top;
        if($.browser.safari){
            $('body').animate( { scrollTop: destination }, 1500 );
        }else{
            $('html').animate( { scrollTop: destination }, 1500 );
        }
        return false;
    });


    $(".menu-btn").on('click',function (e) {
        e.preventDefault();
        $('.mobile-menu').show();
    });
    $(".mobile-menu").on('click',function (e) {
        e.stopPropagation();
        $('.mobile-menu').hide();
    });

    $( "#accordion" ).accordion();
    $('.owl-carousel').owlCarousel({
        margin: 20,
        items:1,
        nav: true,
        autoHeight: false,
        navText: [
            "<i class='fa fa-chevron-left'></i>",
            "<i class='fa fa-chevron-right'></i>"
        ],
        loop:true,
        responsiveClass:true,
    });
});

