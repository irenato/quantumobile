/**
 * Created by macbook on 21.05.17.
 */


jQuery(document).ready(function ($) {
    $('#contact_form').submit(function () {
        var form = $(this);
        $(this).find('p.error-message').hide();
        if(checkName($(this).find('input[name=name]')) && checkEmail($(this).find('input[name=email]')) && checkEmpty($(this).find('textarea'))){
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'sendContactData',
                    'data': $(this).serialize()
                },
                success: function(response){
                    $(form).find('input, textarea').val('');
                },
            })
        }
        return false;
    })

    function checkEmail(that) {
        var rv_mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if ($(that).val() !== '' && rv_mail.test($(that).val())) {
            $(that).closest('div').find('p.error-message').hide();
            return true;
        } else {
            $(that).closest('div').find('p.error-message').show();
            return false;
        }
    }

    function checkPhone(that) {
        var rv_name = /^\+38\(0[0-9]{2}\) [0-9]{3}\-[0-9]{2}\-[0-9]{2}?_?$/;

        if (rv_name.test($(that).val())) {
            $(that).closest('div').find('p.error-message').hide();
            return true;
        } else {
            $(that).closest('div').find('p.error-message').show();
            return false;
        }
    }

    function checkName(that) {
        var rv_name = /[a-zA-Zа-яА-я]+(\W+)?(\s+)?/;
        if ($(that).val().length > 2 && $(that).val() !== '' && rv_name.test($(that).val())) {
            $(that).closest('div').find('p.error-message').hide();
            return true;
        } else {
            $(that).closest('div').find('p.error-message').show();
            return false;
        }
    }

    function checkEmpty(that){
        if($(that).val().length > 3){
            $(that).closest('div').find('p.error-message').hide();
            return true;
        } else {
            $(that).closest('div').find('p.error-message').show();
            return false;
        }
    }
})

