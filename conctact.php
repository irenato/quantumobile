<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 22:23
 */

/**
 * Template name: Contact
 */

get_header();

?>


<?php if (have_posts()) : while (have_posts()) :
    the_post(); ?>

    <section class="form-white">
        <div class="wrapper">
            <div class="section-title tdark">
                <h2><?php the_title() ?></h2>
                <p>
                    <?php the_content() ?>
                </p>
            </div>
            <?php get_template_part('template-parts/form-contact') ?>
        </div>
    </section>

<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_postdata() ?>

    <section class="contacts">
        <div class="wrapper">
            <ul class="contacts-list">
                <li>
                    <a href="<?= get_option('address_gm'); ?>" target="_blank">
                        <img src="<?= get_template_directory_uri() ?>/images/checkin.png"
                             alt=""><?= get_option('address'); ?>
                    </a>
                </li>
                <li>
                    <a href="tel:<?= sanitazePhone(get_option('phone1')) ?>">
                        <img src="<?= get_template_directory_uri() ?>/images/phone.png"
                             alt=""><?= get_option('phone1'); ?>
                    </a>
                </li>
                <li>
                    <a href="mailto:<?= get_option('admin_email') ?>">
                        <img src="<?= get_template_directory_uri() ?>/images/mail.png" alt="">
                        <?= get_option('admin_email') ?>
                    </a>
                </li>
            </ul>
        </div>
    </section>
    <section class="map">
        <iframe src="<?= get_field('google_maps_link') ?>"
                width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
    </section>

<?php

get_footer();
