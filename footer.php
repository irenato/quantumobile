<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 18:18
 */

?>

<footer>
    <div class="wrapper">
        <div class="block-row">
            <div class="contacts">
                <img src="<?= get_option('footer_logo') ?>" alt="quantu mobile">
                <ul>
                    <li><a href="<?= get_option('admin_email') ?>"><?= get_option('admin_email') ?></a></li>
                    <li><a href="<?= get_option('address_gm') ?>" target="_blank"><?= get_option('address') ?></a></li>
                    <li><a href="tel:<?= sanitazePhone(get_option('phone1')) ?>"><?= get_option('phone1') ?></a></li>
                </ul>
            </div>
            <?php get_template_part('template-parts/footer_menu') ?>
            <ul class="social-links">
                <?php if(get_option('link_lnk')) : ?>
                <li><a href="<?= get_option('link_lnk') ?>" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                <?php endif; ?>
                <?php if(get_option('link_fb')) : ?>
                <li><a href="<?= get_option('link_fb') ?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                <?php endif; ?>
            </ul>
        </div>
        <p class="copywriter">(c) <?= get_bloginfo('name') ?>, <?= date('Y') ?></p>
    </div>
</footer>

<?php get_template_part('template-parts/mobile_menu') ?>

<?php wp_footer(); ?>

<script>
    new WOW().init();
</script>
</body>
</html>
