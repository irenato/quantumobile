<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 22:55
 */

get_header();

?>

<?php if (have_posts()) : while (have_posts()) :
    the_post(); ?>

    <section class='top-block-40' style="background-image: url(<?= get_the_post_thumbnail_url() ?>)">
        <div class="wrapper">
            <div class="date-autor">
                <a href="<?php the_permalink(18); ?>" class="back-link"><i class="fa fa-chevron-left" aria-hidden="true"></i> back</a>
                <span class="news-date"><?= get_the_date('F d, Y') ?></span>
                <span class="news-autor"><?php the_author_meta('display_name'); ?></span>
            </div>
            <h1><?php the_title() ?></h1>
        </div>
    </section>
    <section class="article">
        <div class="wrapper">
            <div class="article-content">
                <?php the_content() ?>
                <img src="<?= get_the_post_thumbnail_url() ?>" alt="<?php the_title(); ?>">
            </div>
            <div class="article-footer">
                <ul class="tags-list">
                    <li>CATEGORIES:</li><?php $category = get_the_category(get_the_ID()); ?>
                    <?php foreach ($category as $item): ?>
                        <li>
                        <a href="<?= get_category_link($item->cat_ID) ?>"
                           class="news-tag <?= get_field('color', 'category_' . $item->cat_ID) ?>"><?= $item->cat_name ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <div class="share-links">
                    <?= DISPLAY_ULTIMATE_PLUS(); ?>
                </div>

            </div>
            <?= get_next_post_link('%link', 'see next post', true); ?>
        </div>
    </section>
<?php endwhile; ?>
<?php endif; ?>
<?php

get_footer();
