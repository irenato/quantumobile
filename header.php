<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 18:18
 */

?>


<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?php wp_head(); ?>


    <title><?= wp_get_document_title('|', true, 'right'); ?></title>
</head>
<body>
<header>
    <div class="wrapper">
        <a href="<?= get_home_url(); ?>" class="logo"><img src="<?= get_option('logo') ?>" alt="<?= get_option('sitename') ?>"></a>
        <?php get_template_part('template-parts/header_menu') ?>
        <a class="mobile menu-btn" style="background-image: url(<?= get_template_directory_uri() ?>/images/menu_mobile.svg);"></a>
    </div>
</header>

<?php

$page_id = $post->ID;
