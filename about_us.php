<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 18:51
 */

/**
 * Template name: About_us
 */

get_header();
?>

<?php if (have_posts()) : while (have_posts()) :
    the_post(); ?>
    <?php $page_id = $post->ID; ?>
    <section class='top-block-services' style="background-image: url(<?= get_the_post_thumbnail_url() ?>)">
        <div class="wrapper">
            <h1><?= get_option('banner_text') ?></h1>
        </div>
    </section>
    <section class="services-tabs">
        <ul class="services-tabs-items wrapper">
            <li class="services-tabs-item">
                <a href="#<?= str_replace(' ', '_', get_field('tab_1_title')) ?>">
                    <?= get_field('tab_1_title') ?>
                </a>
            </li>
            <li class="services-tabs-item">
                <a href="#<?= str_replace(' ', '_', get_field('tab_2_title')) ?>">
                    <?= get_field('tab_2_title') ?>
                </a>
            </li>
            <li class="services-tabs-item">
                <a href="#<?= str_replace(' ', '_', get_field('tab_3_title')) ?>">
                    <?= get_field('tab_3_title') ?>
                </a>
            </li>
        </ul>
    </section>
    <section class="article" id="<?= str_replace(' ', '_', get_field('tab_1_title')) ?>">
        <div class="wrapper">
            <div class="article-content">
                <h2><?php the_title() ?></h2>
                <?php the_content(); ?>
            </div>
        </div>
    </section>
    <section class="why-ourteam">
        <h2><?= get_field('title_advantages_block') ?></h2>
        <?php $advantage = get_field('description_advantages_block') ?>
        <?php if ($advantage): ?>
            <ul class="advantages-list">
                <?php foreach ($advantage as $item): ?>
                    <li>
                        <h3><?= $item['title'] ?></h3>
                        <p>
                            <?= $item['description'] ?>
                        </p>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </section>
    <section class="career" id="<?= str_replace(' ', '_', get_field('tab_2_title')) ?>">
    <div class="wrapper">
    <div class="section-title tdark">
        <h2><?= get_field('title_vacancies_block') ?></h2>
        <p>
            <?= get_field('description_vacancies_block') ?>
        </p>
    </div>
<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_postdata() ?>
<?php $args = array(
    'offset' => 0,
    'post_type' => 'vacancies',
    'posts_per_page' => -1); ?>
<?php $vacancies = new WP_query($args); ?>
<?php if ($vacancies->have_posts()): ?>
    <div class="vacancies">
        <h3>open vacancies:</h3>
        <table>
            <tbody>
            <?php while ($vacancies->have_posts()) : $vacancies->the_post(); ?>
                <tr>
                    <td><a href="<?= get_the_permalink(); ?>"><?php the_title() ?></a></td>
                    <td><?= get_field('location') ?></td>
                    <td><?= get_field('type') ?></td>
                </tr>
            <?php endwhile; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>
    </div>
    </section>
    <section class="form" id="<?= str_replace(' ', '_', get_field('tab_3_title')) ?>"
             style="background-image: url(<?= get_field('image_contact', $page_id) ?>)">
        <div class="wrapper">
            <div class="section-title tlight">
                <h2><?= get_field('title_contact', $page_id) ?></h2>
                <p>
                   <?= get_field('description_contact', $page_id) ?>
                </p>
            </div>
           <?php get_template_part('template-parts/form-contact') ?>
        </div>
    </section>

<?php

get_footer();
