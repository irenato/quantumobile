<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 27.05.17
 * Time: 16:16
 */

?>

<form action="/" method="post" id="contact_form">
    <div class="form-item-name">
        <input type="text" name="name" id="name" placeholder="your name*" required>
        <label for="name">your name*</label>
        <p class="error-message">Name error</p>
    </div>
    <div class="form-item-email">
        <input type="text" id="email" name="email" placeholder="your email*" required>
        <label for="email">your email*</label>
        <p class="error-message">Email error</p>
    </div>
    <div class="form-item-message">
                    <textarea name="message" id="meessage" cols="30" rows="10" placeholder="your message*"
                              required></textarea>
        <label for="meessage">your message*</label>
        <p class="error-message">Message error</p>
    </div>
    <div class="form-item">
        <button type="submit">sent message</button>
    </div>
</form>
