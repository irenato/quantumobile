<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 24.05.17
 * Time: 23:35
 */


wp_nav_menu(array(
    'theme_location' => '',
    'menu' => 6,
    'container' => false,
    'container_class' => '',
    'container_id' => '',
    'menu_class' => 't_nav',
    'menu_id' => '',
    'echo' => true,
    'fallback_cb' => 'wp_page_menu',
    'before' => '',
    'after' => '',
    'link_before' => '',
    'link_after' => '',
    'items_wrap' => '<ul id="%1$s" class="%2$s menu">%3$s</ul>',
    'depth' => 0,
    'walker' => '',
));