<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 18:31
 */

?>

<section class="modal">
    <div class="mobile-menu">
        <div class="wrapper">
            <?php
            $args = array(
                'order' => 'ASC',
                'orderby' => 'menu_order',
                'output' => ARRAY_A,
                'output_key' => 'menu_order',
                'update_post_term_cache' => false,
            );
            $items = wp_get_nav_menu_items(5, $args);
            /*
             * Это безусловно треш,
             * но как иначе прикрутить картинки в svg мне пока не известно
             */
            $images = array('/images/home.svg', '/images/services.svg', '/images/aboutUS.svg', '/images/blog.svg', '/images/contacts.svg') ?>
            <?php if ($items): ?>
            <ul>

                <?php $i = -1; ?>
                <?php foreach ($items as $item): ?>
                    <?php ++$i; ?>
                    <li><a href="<?= get_permalink($item->ID) ?>" <?= $item->ID == $page_id ?  'class="active"' : '' ?>>
                            <img src="<?= get_template_directory_uri() . $images[$i] ?>" alt="<?= $item->post_title ?>"><?= $item->title ?>
                        </a>
                    </li>
                <?php endforeach; ?>
                <?php endif; ?>
        </div>
    </div>
</section>
