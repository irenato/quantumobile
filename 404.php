<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 18:18
 */

get_header();

?>

    <section class='top-block-40' style="background-image: url(<?= get_the_post_thumbnail_url() ?>)">
        <div class="wrapper">
            <div class="date-autor">
                <a href="<?= get_home_url(); ?>" class="back-link"><i class="fa fa-chevron-left"
                                                                           aria-hidden="true"></i> back</a>
            </div>
            <h1>404</h1>
        </div>
    </section>

<?php

get_footer();