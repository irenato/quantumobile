<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 18:15
 */

if (is_front_page()) {
    get_template_part('home');
} else if (is_single()) {
    get_template_part('single');
}