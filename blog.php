<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 22:53
 */

/**
 * Template name: Blog
 */

get_header();

?>

<?php if (have_posts()) : while (have_posts()) :
    the_post(); ?>

    <section class='top-block-services' style="background-image: url(<?= get_the_post_thumbnail_url() ?>)">
        <div class="wrapper">
            <h1><?php the_title() ?></h1>
        </div>
    </section>

    <section class="resent-news">
    <div class="wrapper">
    <div class="section-title tdark">
        <p>
            <?php the_content() ?>
        </p>
    </div>
<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_postdata() ?>
<?php $paged = get_query_var('paged') ? get_query_var('paged') : 1; ?>
    <ul class="news-prev">
        <?php $args = array(
            'orderby' => 'ID desc',
            'paged' => $paged,
            'posts_per_page' => 6
        ); ?>
        <?php $posts = new WP_query($args); ?>
        <?php while ($posts->have_posts()) : $posts->the_post(); ?>
            <li class="news-prev-item">
                <div class="item-img">
                    <img src="<?= get_the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
                    <?php $category = get_the_category($posts->ID); ?>
                    <?php foreach ($category as $item): ?>
                        <a href="<?= get_category_link($item->cat_ID) ?>"
                           class="news-tag <?= get_field('color', $item->cat_ID) ?>"><?= $item->cat_name ?></a>
                    <?php endforeach; ?>
                </div>
                <div class="date-autor">
                    <span class="news-date"><?= get_the_date('F d, Y') ?></span>
                    <span class="news-autor"><?php the_author_meta('display_name'); ?></span>
                </div>
                <div class="description">
                    <h3 class="dtitle"><?php the_title() ?></h3>
                    <p>
                        <?= the_excerpt_max_charlength(); ?>
                    </p>
                </div>
                <a href="<?php the_permalink(); ?>" class="read-more-link">read more</a>
            </li>
        <?php endwhile; ?>
    </ul>
        <?php previous_posts_link('see previous posts', $posts->max_num_pages); ?>
        <?php next_posts_link('see more posts', $posts->max_num_pages); ?>
    </div>
    </section>

<?php
get_footer();
