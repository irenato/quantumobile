<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 18:15
 */

include('functions/scripts_styles.php');
include('functions/q_functions.php');
include('functions/custom_posts.php');
include('functions/hidden.php');
include('functions/q_ajax.php');

if (is_admin()) {
    include('functions/admin.php');
    include('functions/admin_functions.php');
}


add_theme_support('post-thumbnails');

function themeslug_theme_customizer($wp_customize)
{
    $wp_customize->add_section('themeslug_logo_section', array(
        'title' => __('Logo', 'themeslug'),
        'priority' => 30,
        'description' => 'Upload a logo to replace the default site name and description in the header',
    ));

    $wp_customize->add_setting('themeslug_logo');
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'themeslug_logo', array(
        'label' => __('Logo', 'themeslug'),
        'section' => 'themeslug_logo_section',
        'settings' => 'themeslug_logo',
    )));
}

add_action('init', 'omyblog_init_session', 1);
if (!function_exists('omyblog_init_session')):
    function omyblog_init_session()
    {
        session_start();
    }
endif;

function my_myme_types($mime_types){
    $mime_types['svg'] = 'image/svg+xml'; // поддержка SVG
    return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);