<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 18:33
 */

get_header();

?>

<?php if (have_posts()) : while (have_posts()) :
    the_post(); ?>
    <?php $page_id = $post->ID; ?>
    <section class='top-block'
             style="background-image: url(<?= get_the_post_thumbnail_url() ?>)">
        <div class="wrapper">
            <?php the_content() ?>
            <a href="<?= trailingslashit(get_field('main_banner_button_link')) ?>"
               class="link-btn blue"><?= get_field('main_banner_button_text') ?></a>

        </div>
    </section>
<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_postdata() ?>

    <section class="services">
        <div class="wrapper">
            <div class="section-title tdark">
                <h2><?= get_field('title_our_services', $page_id); ?></h2>
                <p>
                    <?= get_field('description_our_services', $page_id); ?>
                </p>
            </div>
            <?php $pages = get_pages(
                array(
                    'meta_key' => '_wp_page_template',
                    'meta_value' => 'services.php',
                    'post_type' => 'page',
                    'post_status' => 'publish',
                    'parent' => -1,
                )
            );
            ?>
            <ul class="services-list">
                <?php foreach ($pages as $post):
                    setup_postdata($post); ?>
                    <li class="services-list-item">
                        <img src="<?= get_field('logo_main') ?>" alt="<?= get_the_title(); ?>">
                        <div class="description">
                            <h3 class="dtitle"><?= get_the_title(); ?></h3>
                            <p>
                                <?= the_excerpt_max_charlength(); ?>
                            </p>
                            <a href="<?= get_the_permalink(); ?>" class="read-more-link">read more</a>
                        </div>
                    </li>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>
            </ul>
        </div>
    </section>


    <section class="about-us">
        <div class="wrapper">
            <div class="section-title tdark">
                <h2><?= get_field('title_about_us', $page_id) ?></h2>
                <p>
                    <?= get_field('introtext_about_us', $page_id) ?>
                </p>
            </div>
            <div class="about-us-block">
                <img src="<?= get_field('image_about_us', $page_id) ?>"
                     alt="<?= get_field('title_about_us', $page_id) ?>">
                <div class="about-us-description">
                    <h3 class="dtitle"><?= get_field('title_about_us_content', $page_id) ?></h3>
                    <p>
                        <?= get_field('description_about_us_content', $page_id) ?>
                    </p>
                    <a href="<?= get_the_permalink(41) ?>" class="link-btn white">read more</a>
                </div>
            </div>
        </div>
    </section>
    <section class="choose-us">
        <div class="wrapper">
            <div class="section-title tdark">
                <h2><?= get_field('title_choose_us', $page_id) ?></h2>
                <p>
                    <?= get_field('description_choose_us', $page_id) ?>
                </p>
            </div>
            <?php $items = get_field('items_choose_us', $page_id); ?>
            <?php if ($items): ?>
                <ul class="arguments-list">
                    <?php $i = 0; ?>
                    <?php foreach ($items as $item): ?>
                        <?php ++$i; ?>
                        <li class="arguments-list-item">
                            <img src="<?= $item['image'] ?>" alt="<?= $item['title'] ?>">
                            <div class="description">
                                <h3 class="dtitle"><?= $item['title'] ?></h3>
                                <p>
                                    <?= $item['description'] ?>
                                </p>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
    </section>

    <section class="clients">
        <div class="section-title tdark">

            <h2><?= get_field('title_clients', $page_id) ?></h2>
            <p>
                <?= get_field('descrip_clients', $page_id) ?>
            </p>
        </div>
        <?php $items = get_field('items_clients', $page_id); ?>
        <?php if ($items): ?>
            <ul class="client-list">
                <?php foreach ($items as $item): ?>
                    <li class="client-list-item">
                        <img src="<?= $item['image'] ?>" alt="<?= $item['alt'] ?>">
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </section>

    <section class="resent-news">
        <div class="wrapper">
            <div class="section-title tdark">
                <h2> <?= get_field('title_posts', $page_id) ?></h2>
                <p>
                    <?= get_field('description_posts', $page_id) ?>
                </p>
            </div>
            <?php $args = array(
                'orderby' => 'ID desc',
                'paged' => $paged,
                'posts_per_page' => 6
            ); ?>
            <?php $posts = new WP_query($args); ?>
            <ul class="news-prev">
                <?php while ($posts->have_posts()) : $posts->the_post(); ?>
                    <li class="news-prev-item">
                        <div class="item-img">
                            <img src="<?= get_the_post_thumbnail_url() ?>" alt="<?= get_the_title() ?>">
                            <?php $category = get_the_category($posts->ID); ?>
                            <?php foreach ($category as $item): ?>
                                <a href="<?= get_category_link($item->cat_ID) ?>"
                                   class="news-tag <?= get_field('color', 'category_' . $item->cat_ID) ?>"><?= $item->cat_name ?></a>
                            <?php endforeach; ?>
                        </div>
                        <div class="date-autor">
                            <span class="news-date"><?= get_the_date('F d, Y') ?></span>
                            <span class="news-autor"><?php the_author_meta('display_name'); ?></span>
                        </div>
                        <div class="description">
                            <h3 class="dtitle"><?php the_title() ?></h3>
                            <p>
                                <?= the_excerpt_max_charlength(); ?>
                            </p>
                        </div>
                        <a href="<?php the_permalink(); ?>" class="read-more-link">read more</a>
                    </li>
                <?php endwhile; ?>
            </ul>
            <?php wp_reset_postdata(); ?>
            <a href="<?php the_permalink(18); ?>" class="link-btn white">see more post</a>
        </div>
    </section>

    <section class="form"
             style="background-image: url(<?= get_field('image_contact', $page_id) ?>)">
        <div class="wrapper">
            <div class="section-title tlight">
                <h2><?= get_field('title_contact', $page_id) ?></h2>
                <p>
                    <?= get_field('description_contact', $page_id) ?>
                </p>
            </div>
            <?php get_template_part('template-parts/form-contact') ?>
        </div>
    </section>

<?php
get_footer();
