<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 18:53
 */

function handle_logo_upload()
{
    if (!empty($_FILES["logo"]["tmp_name"])) {
        $urls = wp_handle_upload($_FILES["logo"], array('test_form' => FALSE));
        $temp = $urls["url"];
        return $temp;
    }
    return get_option('logo');

}

function handle_footer_logo_upload()
{
    if (!empty($_FILES["footer_logo"]["tmp_name"])) {
        $urls = wp_handle_upload($_FILES["footer_logo"], array('test_form' => FALSE));
        $temp = $urls["url"];
        return $temp;
    }
    return get_option('footer_logo');

}

/**
 *
 */
function display_theme_panel_fields()
{
    add_settings_section("section", "", null, "theme-options");
    add_settings_field("phone1", "Phone number", "display_phone1_element", "theme-options", "section");
//    add_settings_field("phone2", "Контактный номер телефона", "display_phone2_element", "theme-options", "section");
//    add_settings_field("phone3", "Контактный номер телефона", "display_phone3_element", "theme-options", "section");
    add_settings_field("skype", "Skype", "display_skype_element", "theme-options", "section");
    add_settings_field("address", "Address", "display_address_element", "theme-options", "section");
    add_settings_field("address_gm", "Address (Google maps)", "display_address_gm_element", "theme-options", "section");
//    add_settings_field("link_vk", "Профиль Вконтакте", "display_link_vk_element", "theme-options", "section");
    add_settings_field("link_lnk", "Linkedin", "display_link_inst_element", "theme-options", "section");
    add_settings_field("link_fb", "Facebook", "display_link_fb_element", "theme-options", "section");
//    add_settings_field("link_odn", "Профиль Одноклассники", "display_link_odn_element", "theme-options", "section");
    add_settings_field("copyright", "Copyright", "display_copyright_element", "theme-options", "section");
    add_settings_field("copyright_description", "Copyright (описание)", "display_copyright_description_element", "theme-options", "section");
    add_settings_field("logo", "Logo", "logo_display", "theme-options", "section");
    add_settings_field("footer_logo", "Logo 2", "footer_logo_display", "theme-options", "section");

    register_setting("section", "phone1");
//    register_setting("section", "phone2");
//    register_setting("section", "phone3");
    register_setting("section", "skype");
    register_setting("section", "address");
    register_setting("section", "address_gm");
//    register_setting("section", "link_vk");
    register_setting("section", "link_lnk");
    register_setting("section", "link_fb");
//    register_setting("section", "link_odn");
    register_setting("section", "copyright");
    register_setting("section", "copyright_description");
    register_setting("section", "logo", "handle_logo_upload");
    register_setting("section", "footer_logo", "handle_footer_logo_upload");
}

add_action("admin_init", "display_theme_panel_fields");

function add_theme_menu_item()
{
    add_menu_page("Theme settings", "Theme settings", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");