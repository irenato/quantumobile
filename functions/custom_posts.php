<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 27.05.17
 * Time: 13:18
 */


//------------------------------------------
// services
//------------------------------------------
add_action('init', 'sections_register');
function sections_register()
{
    register_post_type('vacancies',
        array(
            'label' => __('Vacancies'),
            'singular_label' => 'vacancy',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-nametag',
            'supports' => array(
                'title',
                'thumbnail',
                'editor',
            ),
        )
    );
}