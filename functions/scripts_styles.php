<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 18:31
 */

function quantum_mobile_styles()
{
    wp_enqueue_style('quantum_mobile-fonts', get_template_directory_uri() . '/css/fonts.min.css', '', '', 'all');
    wp_enqueue_style('quantum_mobile-heade', get_template_directory_uri() . '/css/header.min.css', '', '', 'all');
    wp_enqueue_style('quantum_mobile-footer', get_template_directory_uri() . '/css/footer.min.css', '', '', 'all');
    wp_enqueue_style('quantum_mobile-carousel_style', get_template_directory_uri() . '/libs/owl.carousel/dist/assets/owl.carousel.min.css', '', '', 'all');
    wp_enqueue_style('quantum_mobile-theme', get_template_directory_uri() . '/libs/owl.carousel/dist/assets/owl.theme.default.min.css', '', '', 'all');
    wp_enqueue_style('quantum_mobile-wow', get_template_directory_uri() . '/libs/wow/css/libs/animate.css', '', '', 'all');
    wp_enqueue_style('quantum_mobile-main', get_template_directory_uri() . '/css/main.min.css', '', '', 'all');
    wp_enqueue_style('quantum_mobile-font_awesome', get_template_directory_uri() . '/css/font-awesome.min.css', '', '', 'all');
}

add_action('wp_enqueue_scripts', 'quantum_mobile_styles');

function quantum_mobile_scripts()
{
    wp_enqueue_script('quantum_mobile-ui', get_template_directory_uri() . '/libs/jquery-ui-1.12.1.custom/jquery-ui.js', false, '', true);
    wp_enqueue_script('quantum_mobile-carousel', get_template_directory_uri() . '/libs/owl.carousel/dist/owl.carousel.min.js', false, '', true);
    wp_enqueue_script('quantum_mobile-wow', get_template_directory_uri() . '/libs/wow/dist/wow.min.js', false, '', true);
    wp_enqueue_script('quantum_mobile-common', get_template_directory_uri() . '/js/common.js', false, '', true);
    wp_enqueue_script('quantum_mobile-action', get_template_directory_uri() . '/js/action.js', false, '', true);
    wp_localize_script('quantum_mobile-action', 'alevel_ajax', array(
        'ajax_url' => admin_url('admin-ajax.php')
    ));
}

add_action('wp_enqueue_scripts', 'quantum_mobile_scripts');