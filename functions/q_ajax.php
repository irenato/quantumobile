<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 27.05.17
 * Time: 19:20
 */

add_action('wp_ajax_nopriv_sendContactData', 'sendContactData');
add_action('wp_ajax_sendContactData', 'sendContactData');

function sendContactData()
{
    $data = array();
    parse_str($_POST['data'], $data);
    $headers = array(
        'MIME-Version: 1.0',
        'From: ' . $data['email'],
        'Reply-To: ' . $data['email'],
        'Content-Type: text/html; charset=utf-8'
    );
    $mail_subject = 'From contact form: ' . $data['name'] . "\r\n";
    $to = get_option('admin_email');
    $mail_text = 'Name: ' . $data['name'] . "\r\n";
    $mail_text .= 'Email: ' . $data['email'] . "\r\n";
    $mail_text .= 'Message: ' . $data['message'] . "\r\n";
    wp_send_json(wp_mail($to, $mail_subject, $mail_text, $headers));
}