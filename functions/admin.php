<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21.05.17
 * Time: 18:53
 */

function theme_settings_page()
{

    echo '<div class="wrap">';
    echo '<h1>Theme settings</h1>';
    echo '<form method="post" action="options.php" enctype="multipart/form-data">';
    settings_fields("section");
    do_settings_sections("theme-options");
    submit_button();
    echo '</form>';
    echo '</div>';
}

function display_phone1_element()
{
    echo '<input type="text" name="phone1" id="phone1" value="' . get_option('phone1') . '"/>';
}

//function display_phone2_element()
//{
//    echo '<input type="text" name="phone2" id="phone2" value="' . get_option('phone2') . '"/>';
//}
//
//function display_phone3_element()
//{
//    echo '<input type="text" name="phone3" id="phone3" value="' . get_option('phone3') . '"/>';
//}

function display_address_element()
{
    echo '<textarea name="address" id="address" cols="45" rows="3">' . get_option('address') . '</textarea>';
}

function display_address_gm_element()
{
    echo '<textarea name="address_gm" id="address_gm" cols="45" rows="3">' . get_option('address_gm') . '</textarea>';
}

function display_link_vk_element()
{
    echo '<input type="text" name="link_vk" id="link_vk" value="' . get_option('link_vk') . '"/>';
}

function display_link_inst_element()
{
    echo '<input type="text" name="link_lnk" id="link_lnk" value="' . get_option('link_lnk') . '"/>';
}

function display_link_fb_element()
{
    echo '<input type="text" name="link_fb" id="link_fb" value="' . get_option('link_fb') . '"/>';
}

function display_link_odn_element()
{
    echo '<input type="text" name="link_odn" id="link_odn" value="' . get_option('link_odn') . '"/>';
}

function display_skype_element()
{
    echo '<input type="text" name="skype" id="skype" value="' . get_option('skype') . '"/>';
}

function display_copyright_element()
{
    echo '<textarea name="copyright" id="copyright" cols="45" rows="3">' . get_option('copyright') . '</textarea>';
}

function display_copyright_description_element()
{
    echo '<textarea name="copyright_description" id="copyright_description" cols="45" rows="3">' . get_option('copyright_description') . '</textarea>';
}


function logo_display()
{
    echo '<input type="file" name="logo"
            accept="image/*"/>';
    echo get_option('logo');
}

function footer_logo_display()
{
    echo '<input type="file" name="footer_logo"
            accept="image/*"/>';
    echo get_option('footer_logo');
}