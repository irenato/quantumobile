<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 24.05.17
 * Time: 23:19
 */

/**
 * Template name: Services
 */

get_header();

?>

<?php if (have_posts()) : while (have_posts()) :
    the_post(); ?>

    <section class='top-block-services' style="background-image: url(<?= get_the_post_thumbnail_url() ?>)">
        <div class="wrapper">
            <h1><?= get_the_title() ?></h1>
        </div>
    </section>
    <section class="services-tabs">
        <?php get_template_part('template-parts/services_menu') ?>
    </section>
    <section class="tab-content">
        <div class="wrapper">
            <div class="section-title tdark">
                <img src="<?= get_field('logo') ?>" alt="<?= get_the_title() ?>">
                <h2><?= get_the_title() ?></h2>
            </div>
            <div class="services-info">
                <?php the_content() ?>
                <?php $description = get_field('description') ?>
                <?php if ($description): ?>
                    <?php $i = 0; ?>
                    <div id="accordion" class="accordion">
                        <?php foreach ($description as $item): ?>
                            <h3><span class="acordion-number"><?= ++$i ?></span><?= $item['title'] ?></h3>
                            <div>
                                <p>
                                    <?= $item['description'] ?>
                                </p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="proj-slider">
        <div class="wrapper">
            <h2><?= get_field('slider_title') ?></h2>
            <?php $slider = get_field('slider'); ?>
            <?php if ($slider): ?>
                <div class="slider owl-carousel owl-theme">
                    <?php foreach ($slider as $item): ?>
                        <div class="slide">
                            <img src="<?= $item['image'] ?>" alt="<?= $item['title'] ?>"/>
                            <div class="description">
                                <h3><?= $item['title'] ?></h3>
                                <?= $item['description'] ?>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            <?php endif; ?>
        </div>
    </section>

<?php endwhile; ?>
<?php endif; ?>
<?php

get_footer();
